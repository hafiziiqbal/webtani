<?php

namespace App\Http\Controllers\API;

use App\Models\Petani;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StorePetaniRequest;
use App\Http\Requests\StoreUpdateRequest;
use App\Models\Kelompok_Tani;

class PetaniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Petani::all();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePetaniRequest $request)
    {
        $validasi = $request->validated();
        try {
            $image = $request->file('foto')->hashName();
            $path = $request->file('foto')->storeAs('uploads/petanis', $image);
            $validasi['foto'] = $path;
            $response = Petani::create($validasi);
            return response()->json([
                'success' => true,
                'message' => 'success',
                'data'    => $response
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Err',
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Petani::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id)
    {
        $validasi = $request->validated();
        $response = Petani::find($id);
        try {
            if ($request->file('foto')) {

                $image = $request->file('foto');
                $path =  $image->storeAs('uploads/petanis', $image->hashName());
                $file_path = public_path() . '/' . $response->foto;
                unlink($file_path);
                $validasi['foto'] = $path;
            }

            $response = Petani::find($id);
            $response->update($validasi);

            return response()->json([
                'success' => true,
                'message' => 'success',
                'data'    => $response
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Err',
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $petani = Petani::find($id);
            $file_path = public_path() . '/' . $petani->foto;
            unlink($file_path);
            $petani->delete();
            return response()->json([
                'succes' => true,
                'message' => 'Success'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Err',
                'errors' => $e->getMessage()
            ]);
        }
    }


    function kelompoktani()
    {
        $data = Kelompok_Tani::all();
        return response()->json($data);
    }
}
