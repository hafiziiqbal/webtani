<?php

namespace App\Http\Controllers\API;


use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRegistrasiRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use Symfony\Component\HttpFoundation\Response;


class AuthController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required',
            'password' => [
                'required', Password::min(size: 6)
                    ->letters()
                    ->numbers()
            ]
        ]);
        return $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password)
        ]);
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user || !\Hash::check($request->password, $user->password)) {
            return response()->json([
                'message' => 'UNAUOTHORIZED'
            ], 401);
        }

        $token = $user->createToken('token')->plainTextToken;
        return response()->json([
            'message'   => 'Success',
            'user'      => $user,
            'token'     => $token
        ], 200);

        // if (!Auth::attempt([
        //     'email'     => $request->email,
        //     'password'  => $request->password
        // ])) {
        //     return response([
        //         'message' => 'invalid credentials'
        //     ], status: Response::HTTP_UNAUTHORIZED);
        // }

        // $user = Auth::user();

        // $token = $user->createToken('token')->plainTextToken;
        // $cookie = cookie(name: 'jwt', value: $token, minutes: 68 * 24); //1 hari

        // return response([
        //     'message' => 'Success'
        // ])->withCookie($cookie);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        // $user->currentAccessToken()->delete();
        $user->tokens()->where('tokenable_id', $user->id)->delete();

        return response()->json([
            'message' => 'Berhasil LogOut'
        ], 200);
    }
}
