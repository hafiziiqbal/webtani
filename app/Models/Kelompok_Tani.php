<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelompok_Tani extends Model
{
    use HasFactory;
    public $primaryKey = 'id_kelompok_tani';
    protected $fillable = [
        'nama_kelompok'
    ];
}
