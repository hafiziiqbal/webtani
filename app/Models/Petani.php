<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Petani extends Model
{
    use HasFactory;
    public $primaryKey = 'id_penjual';
    protected $fillable = [
        'nama', 'nik', 'alamat', 'telp', 'foto', 'id_kelompok_tani', 'status'
    ];
    static function getPetani()
    {
        $return = DB::table('petanis')
            ->join('kelompok__tanis', 'petanis.id_kelompok_tani', '=', 'kelompok__tanis.id_kelompok_tani');
        return $return;
    }
}
